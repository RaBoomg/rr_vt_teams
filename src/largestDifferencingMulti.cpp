#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <functional>

#include "Person.h"
#include "Team.h"
// std::list<int32_t> TestNumbers = {165, 52, 281, 204, 59, 300, 152, 163, 283, 159, 168, 44, 289, 182, 206, 74, 276, 111, 161, 236, 268, 211, 9, 92, 194, 34, 177, 199, 190, 242, 107, 151, 292, 75, 121, 104, 3, 77, 23, 94, 148, 197, 176, 259, 135, 33, 113, 49, 39, 82, 97, 294, 274, 227, 42, 173, 142, 28, 54, 61, 216, 96, 143, 1, 221, 103, 196, 26, 258, 62, 105, 295, 35, 90, 282, 110, 205, 215, 83, 20, 109, 89, 278, 203, 284, 41, 267, 223, 150, 127, 38, 156, 155, 27, 266, 225, 37, 249, 240, 11};
// std::list<int32_t> TestNumbers = {165, 52, 281, 204, 59, 300, 152, 163, 283, 159, 168, 44, 289, 182, 206, 74, 276, 111, 161, 236, 268, 211, 9, 92, 194, 34, 177, 199, 190, 242, 107, 151, 292, 75, 121, 104, 3, 77, 23, 94, 148, 197, 176, 259, 135, 33, 113, 49, 39, 82, 97, 294, 274, 227, 42, 173, 142, 28, 54, 61, 216, 96, 143, 1, 221, 103, 196, 26, 258, 62, 105, 295, 35, 90, 282, 110, 205, 215, 83, 20, 109, 89, 278, 203, 284, 41, 267, 223, 150, 127, 38, 156, 155, 27, 266, 225, 37, 249, 240};
// std::list<int32_t> TestNumbers = {109, 109, 103, 108, 101, 109, 100, 108, 107, 102, 106, 101, 108, 101, 106, 105, 104, 103, 105, 107, 103, 109, 108, 108, 107, 107, 110, 102, 108, 103, 110, 104, 102, 110, 105, 108, 103, 109, 106, 100, 100, 100, 102, 104, 107, 101, 100, 107, 109, 106, 102, 106, 102, 108, 108, 101, 106, 107, 104, 101, 105, 108, 109, 101, 110, 106, 110, 110, 103, 102, 108, 107, 102, 109, 100, 104, 109, 101, 101, 106, 106, 106, 108, 102, 101, 104, 104, 101, 100, 106, 100, 106, 102, 110, 100, 101, 104, 103, 107, 107, 107, 104, 100, 100, 109, 108, 100, 109, 101, 104, 109, 100, 103, 110, 101, 103, 109, 109, 102, 104, 101};
// std::list<int32_t> TestNumbers = {8, 7, 6, 5, 4};

void multiWayPartitioning(std::list<Person> inputList, int32_t numberOfTeams, std::list<std::list<Person>> &returnLists)
{
    // https://en.wikipedia.org/wiki/Largest_differencing_method#Multi-way_partitioning
    // std::sort(inputList.begin(), inputList.end(), std::greater<int32_t>());

    std::list<std::list<std::list<Person>>> workingLists;
    for (auto i = inputList.begin(); i != inputList.end(); ++i)
    {
        std::list<std::list<Person>> pushbackList;
        // std::cout << *i << std::endl;
        std::list<Person> thing = {*i};
        pushbackList.push_back(thing);
        for (int j = 1; j < numberOfTeams; ++j)
        {
            pushbackList.push_back(std::list<Person>{});
        }
        workingLists.push_back(pushbackList);
    }

    // auto sumPerson = [](Person left, Person right)
    // { return static_cast<int32_t>(left.getStrength() + right.getStrength()); };
    // std::cout << "Before while" << std::endl;
    while (workingLists.size() > 1)
    {
        // std::cout << "Working lists size " << workingLists.size() << std::endl;
        // std::cout << "Working lists(0) size " << workingLists.front().size() << std::endl;
        // std::cout << "Working lists(0)(0) size " << workingLists.front().front().size() << std::endl;
        // Go through and calculate the sums of the innermost lists
        // calculate the difference between the largest and smallest innermost list for each second-innermost list
        auto largestIterator = workingLists.begin();
        int32_t largestValue = 0;
        auto secondLargestIterator = workingLists.begin();
        int32_t secondLargestValue = 0;

        for (auto i = workingLists.begin(); i != workingLists.end(); ++i)
        {
            int32_t maxSum = 0;
            int32_t minSum = INT_MAX;

            for (auto j = i->begin(); j != i->end(); ++j)
            {

                // int32_t tempVal = std::accumulate(j->begin(), j->end(), 0, sumPerson);
                int32_t tempVal = 0;
                for (auto pers : *j)
                {
                    tempVal += pers.getStrength();
                }
                if (tempVal > maxSum)
                    maxSum = tempVal;
                if (tempVal < minSum)
                    minSum = tempVal;
            }
            if ((maxSum - minSum) > largestValue)
            {
                secondLargestIterator = largestIterator;
                secondLargestValue = largestValue;
                largestIterator = i;
                largestValue = maxSum - minSum;
            }
            else if ((maxSum - minSum) > secondLargestValue)
            {
                secondLargestIterator = i;
                secondLargestValue = maxSum - minSum;
            }
        }

        // Combine the second-innermost lists which have the biggest difference
        // in reverse order of sizes

        // std::cout << "Before Sorting" << std::endl;

        // std::cout << largestIterator->begin()->front() << std::endl;
        largestIterator->sort([](std::list<Person> a, std::list<Person> b) -> bool
                              {
                                int32_t sum1 = 0;
                                for (auto pers : a)
                                {
                                    sum1 += pers.getStrength();
                                }
                                int32_t sum2 = 0;
                                for (auto pers : b)
                                {
                                    sum2 += pers.getStrength();
                                } 
                                return sum1 > sum2; });
        // std::cout << "after Sorting first" << std::endl;
        secondLargestIterator->sort([](std::list<Person> a, std::list<Person> b) -> bool
                                    {
                                        int32_t sum1 = 0;
                                        for (auto pers : a)
                                        {
                                            sum1 += pers.getStrength();
                                        }
                                        int32_t sum2 = 0;
                                        for (auto pers : b)
                                        {
                                            sum2 += pers.getStrength();
                                        } 
                                        return sum1 < sum2; });
        // std::cout << "after  Sorting second" << std::endl;
        // std::cout << "combining start" << std::endl;
        // std::cout << "largest: " << largestIterator->size();
        // std::cout << "seclargest: " << secondLargestIterator->size();
        auto i = largestIterator->begin();
        for (auto j = secondLargestIterator->begin(); i != largestIterator->end() && j != secondLargestIterator->end(); ++i, ++j)
        {
            // std::cout << "adding " << j->size() << " elements" << std::endl;

            // for (auto k = j->begin(); k != j->end(); ++k)
            // {
            //     i->push_back(*k);
            // }
            for (auto k : *j)
            {
                // std::cout << k.getStrength() << std::endl;
                i->push_back(k);
            }
        }
        // std::cout << "combining done" << std::endl;
        workingLists.erase(secondLargestIterator);
    }

    returnLists = workingLists.front();

    return;
}

// int main()
// {
//     std::cout << "Hello world" << std::endl;
//     std::list<std::list<int32_t>> outList;
//     // twoWayPartitioning(TestNumbers, outList1, outList2);

//     multiWayPartitioning(TestNumbers, 2, outList);
//     for (auto i : outList)
//     {
//         std::cout << std::accumulate(i.begin(), i.end(), 0) << std::endl;
//         std::cout << i.size() << std::endl;
//         int32_t sum1 = 0;
//         for (auto j = i.begin(); j != i.end(); j++)
//         {
//             sum1 += *j - 100;
//         }
//         std::cout << sum1 << std::endl
//                   << 1.0 * sum1 / i.size() << std::endl
//                   << std::endl;
//     }
//     return 0;
// }