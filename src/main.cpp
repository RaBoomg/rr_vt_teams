#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>
#include <ctime>
#include <cmath>
#include <sstream>
#include <list>
#include <filesystem>

#include "Person.h"
#include "Team.h"
#include "largestDifferencingMulti.h"

void changeNumberOfTeams(void);
void printTeams(void);
bool comparePersons(Person &, Person &);
void createTeams(void);
void readInCSV(void);
void writeCSV(void);

std::list<Person> men;
std::list<Person> women;
std::list<std::list<Person>> teams;

uint32_t personsPerTeam = 7;
uint32_t numberOfTeams = 0;

int main(int argc, char *argv[])
{
	int i;

	do
	{
		std::cout << "Was moechten sie machen?" << std::endl;
		std::cout << "0 eingeben, um das Programm zu beenden" << std::endl;
		std::cout << "1 eingeben, um eine .csv Datei einzulesen" << std::endl;
		std::cout << "2 eingeben, um die Anzahl an Teams zu aendern" << std::endl;
		std::cout << "3 eingeben, um eine Teamliste zu erstellen und zu speichern\n  (bestehende Liste wird ueberschrieben)" << std::endl;
		std::cout << "4 eingeben, um die Teamliste aus zu geben" << std::endl; //"4 eingeben, um die Daten einer Person zu aendern"<<endl;
		std::cout << "5 eingeben, verfuegbare Dateien anzuzeigen" << std::endl;
		// std::cout << "5 Ohne Funktion" << std::endl;							 //"5 eingeben, um alle Daten aus zu geben"<<endl;
		std::cin >> i;

		std::filesystem::path myPath = std::filesystem::current_path();

		switch (i)
		{
		case 0:
			break;
		case 1:
			readInCSV();
			break;
		case 2:
			changeNumberOfTeams();
			break;
		case 3:
			createTeams();
			// printTeams();
			writeCSV();
			break;
		case 4:
			printTeams();
			break;
		case 5:
			for (const auto &entry : std::filesystem::directory_iterator(myPath))
			{
				if (entry.path().extension() == ".csv")
					// std::cout << entry.path().extension() << std::endl;
					std::cout << entry.path() << std::endl;
			}
			break;
		default:
			std::cout << "Dies ist keine Option." << std::endl;
			break;
		}
	} while (i != 0);

	return 0;
}

void changeNumberOfTeams(void)
{
	uint32_t tempTeamNum = 0;
	std::cout << "Wie viele Teams sollen es denn sein? (Statt " << numberOfTeams << ")" << std::endl;
	std::cin >> tempTeamNum;
	numberOfTeams = tempTeamNum;
	std::cout << "Alles klar, " << numberOfTeams << " Teams, und los!" << std::endl;
}

bool comparePersons(Person &eins, Person &zwei)
{
	if (eins.getStrength() < zwei.getStrength())
	{
		return true;
	}
	else
		return false;
}

void printTeams(void)
{
	// TODO: could be more generic with writeTeamsCSV
	if (teams.size() == 0)
	{
		std::cout << "Bitte erst Teams erstellen!" << std::endl
				  << std::endl;
		return;
	}

	uint32_t teamNum = 0;
	for (auto team : teams)
	{
		std::cout << "Team " << ++teamNum << ":" << std::endl;
		for (auto pers : team)
		{
			pers.print();
		}
		std::cout << std::endl;
	}
}

void createTeams(void)
{ // Multi-way partitioning using largest differencing method

	// std::cout << "Starting for men" << std::endl;
	// first for men
	std::list<std::list<Person>> maleTeamList;
	multiWayPartitioning(men, numberOfTeams, maleTeamList);
	// second for women
	// std::cout << "Starting for women" << std::endl;
	std::list<std::list<Person>> femaleTeamList;
	multiWayPartitioning(women, numberOfTeams, femaleTeamList);
	// std::cout << "done for women" << std::endl;

	// combine them
	// sort first, but one inversed to the other
	maleTeamList.sort([](std::list<Person> a, std::list<Person> b) -> bool
					  {
                                int32_t sum1 = 0;
                                for (auto pers : a)
                                {
                                    sum1 += pers.getStrength();
                                }
                                int32_t sum2 = 0;
                                for (auto pers : b)
                                {
                                    sum2 += pers.getStrength();
                                } 
                                return sum1 > sum2; });
	// std::cout << "done sort 1" << std::endl;
	femaleTeamList.sort([](std::list<Person> a, std::list<Person> b) -> bool
						{
                                        int32_t sum1 = 0;
                                        for (auto pers : a)
                                        {
                                            sum1 += pers.getStrength();
                                        }
                                        int32_t sum2 = 0;
                                        for (auto pers : b)
                                        {
                                            sum2 += pers.getStrength();
                                        } 
                                        return sum1 < sum2; });

	// combine the lists
	auto i = maleTeamList.begin();
	for (auto j = femaleTeamList.begin(); i != maleTeamList.end() && j != femaleTeamList.end(); ++i, ++j)
	{
		for (auto k = j->begin(); k != j->end(); ++k)
		{
			i->push_back(*k);
		}
	}

	// Calculating and printing some statistics
	int32_t overallStrengthSum = 0;
	int32_t overallAmount = 0;
	std::list<double> teamStrengths;
	std::vector<uint32_t> teamSizes;
	for (auto team : maleTeamList)
	{
		int32_t strengthSum = 0;
		for (auto pers : team)
		{
			strengthSum += pers.getStrength();
			overallAmount++;
		}
		overallStrengthSum += strengthSum;
		teamStrengths.push_back(strengthSum * 1.0 / team.size());
		teamSizes.push_back(team.size());
	}
	double overallAverage = overallStrengthSum * 1.0 / overallAmount;
	double biggestDifference = 0;

	int32_t j = 0;
	for (auto teamStr : teamStrengths)
	{
		double averageDiff = std::abs(overallAverage - teamStr);
		std::cout << "Team " << j++ << std::endl
				  << "Anzahl mitspieler: "
				  << teamSizes.at(j - 1) << std::endl
				  << "Mittlere Staerke: " << teamStr << std::endl
				  << "Abweichung zum Gesamtmittel: " << averageDiff << std::endl
				  << std::endl;

		if (averageDiff > biggestDifference)
		{
			biggestDifference = averageDiff;
		}
	}

	std::cout << "Groesster Abstand zum Gesamtmittel: " << biggestDifference << std::endl
			  << std::endl;

	teams = maleTeamList;
}

void readInCSV(void)
{
	std::string filenameNum;
	std::string line;
	std::string firstName, lastName, strength, gender;
	std::ifstream in;
	std::vector<std::filesystem::path> availableFiles;

	// get all the files in the current working directory to offer them to the user
	std::filesystem::path myPath = std::filesystem::current_path();
	for (const auto &entry : std::filesystem::directory_iterator(myPath))
	{
		if (entry.path().extension() == ".csv")
			availableFiles.push_back(entry.path());
	}

	std::cout << "Verfuegbare Dateien: " << std::endl;
	for (int i = 0; i < availableFiles.size(); ++i)
		std::cout << i + 1 << ": " << availableFiles.at(i) << std::endl;

	std::cout << "\nWelche Datei soll verwendet werden? 0 zum abbrechen verwenden" << std::endl;

	// std::cout
	// 	<< "Wie heisst die Datei (mit Dateiendung)? \nBitte im selben Verzeichnis wie das Programm ablegen "
	// 	<< "mit den Spalten Vorname, \nNachname und Staerke, Maennlich, Weiblich mit Ueberschriften." << std::endl
	// 	<< "Von diesem Programm wird eine mit ',' getrennte .csv-Datei erwartet." << std::endl
	// 	<< "Wenn keine Staerke angegeben wurde, wird die Person uebersprungen, da sie nicht mitspielt." << std::endl;
	// TODO: überarbeiten

	bool repeat = false;
	do
	{
		repeat = false;
		std::cin >> filenameNum;

		if (filenameNum == "0")
			return;

		if (std::stoi(filenameNum) > availableFiles.size())
		{
			std::cout << "So viele Dateien gibt es nicht, bitte noch mal probieren." << std::endl;
			repeat = true;
			continue;
		}

		in.open(availableFiles.at(std::stoi(filenameNum) - 1).c_str(), std::ios::in);

		if (!in.is_open())
		{
			std::cout << "Datei nicht gefunden! Bitte nochmal versuchen! (0 eingeben zum abbrechen)" << std::endl;
		}

	} while (!in || repeat);

	getline(in, line); // skip heading

	while (!in.eof())
	{
		getline(in, line); // get line

		if (!in.eof())
		{
			std::stringstream lineStream(line);
			getline(lineStream, firstName, ',');
			getline(lineStream, lastName, ',');
			getline(lineStream, gender, ',');
			getline(lineStream, strength);

			// std::cout << firstName << " " << lastName << " " << gender << " " << strength << std::endl;

			if (strength == "/" || strength == " " || strength == "")
			{
				continue; // skip this person, as they will not be playing
			}
			int32_t strengthInt = stoi(strength);
			if (strengthInt > 10)
				strengthInt = 10;

			strengthInt += 100;
			if (gender == "Ranger")
			{
				men.push_back(Person(firstName, lastName, strengthInt, true));
			}
			else if (gender == "Rangerin")
			{
				women.push_back(Person(firstName, lastName, strengthInt, false));
			}
			else
			{
				std::cout << "Es gab ein Problem mir der Geschlechtserkennung... (nicht angegeben?)" << std::endl;
				std::cout << "Spieler ohne angegebenes Geschlecht werden nicht in die Teamverteilung aufgenommen," << std::endl;
				std::cout << "deswegen bitte korrigieren und noch mal ausf�hren." << std::endl;
				std::cout << "Name:" << firstName << ' ' << lastName << std::endl;
			}
		}
	}
	numberOfTeams = (men.size() + women.size()) / personsPerTeam;

	std::cout << "Erfolg!" << std::endl
			  << std::endl;
	std::cout << "Die Standardanzahl an Teams waere " << numberOfTeams << ". Fuers aendern ins Menue gucken." << std::endl;

	in.close();
}

void writeCSV(void)
{
	if (teams.size() == 0)
	{
		std::cout << "Bitte erst Teams erstellen!" << std::endl
				  << std::endl;
		return;
	}

	std::ofstream file("teams.csv", std::ios::out | std::ios::trunc);

	if (file.is_open())
	{
		// for (int i = 0; i < teams.size(); i++)
		// {
		// 	file << std::endl
		// 		 << "Team;" << i + 1 << ":; " << std::endl;
		// 	teams.at(i).writeCSV(file);
		// }
		uint32_t teamNum = 0;
		for (auto team : teams)
		{
			file << "Team," << ++teamNum << ": ," << std::endl;
			for (auto pers : team)
			{
				pers.writePersonCSV(file);
			}
			file << std::endl;
		}

		std::cout << "Hat geklappt! teams.csv erstellt" << std::endl
				  << std::endl;
		file.close();
	}
}

/* ToDo:
-schiris realisieren
-doppelnamen (vor und nach) -> geht mit csv
*/