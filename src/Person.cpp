#include <iostream>
#include <fstream>

#include "Person.h"

Person::Person(void)
{
	firstName = "";
	lastName = "";
	strength = 0;
	male = true;
}

Person::Person(std::string _firstName, std::string _lastName, int _strength, bool _male)
{
	firstName = _firstName;
	lastName = _lastName;
	strength = _strength;
	male = _male;
}

Person::~Person(void)
{
}

void Person::writePerson(std::ofstream &file)
{
	if (male == true)
	{
		file << firstName << " " << lastName << (firstName.size() + lastName.size() < 15 ? "\t\t" : "\t") << strength - 100 << "\t\t" << 'x' << std::endl;
	}
	else
	{
		file << firstName << ' ' << lastName << (firstName.size() + lastName.size() < 15 ? "\t\t" : "\t") << strength - 100 << "\t\t  " << 'x' << std::endl;
	}
}

void Person::writePersonCSV(std::ofstream &file, char seperator)
{
	file << firstName << seperator << lastName << seperator << strength - 100 << seperator;
	if (male == true)
	{
		file << "Ranger" << std::endl;
	}
	else
	{
		file << "Rangerin" << std::endl;
	}
}

void Person::print(void)
{
	std::cout << firstName << ' ' << lastName << (firstName.size() + lastName.size() < 15 ? "\t\t" : "\t") << strength - 100 << "\t\t";
	if (male == true)
	{

		std::cout << "Ranger" << std::endl;
	}
	else if (male == false)
	{
		std::cout << "Rangerin" << std::endl;
	}
}

/*****Get and Set methods*****/

int Person::getStrength(void)
{
	return strength;
}

void Person::setStrength(int _strength)
{
	strength = _strength;
}

std::string Person::getFirstName(void)
{
	return firstName;
}

std::string Person::getLastName(void)
{
	return lastName;
}

bool Person::isMale(void)
{
	return male;
}

void Person::setFirstName(std::string _firstName)
{
	firstName = _firstName;
}

void Person::setLastName(std::string _lastName)
{
	lastName = _lastName;
}

void Person::setIsMale(bool _male)
{
	male = _male;
}