#include <vector>
#include <iostream>
#include <fstream>

#include "Person.h"
#include "Team.h"

Team::Team(void)
{
}

Team::~Team(void)
{
}

void Team::add(Person &pers)
{
	members.push_back(pers);
}

void Team::print(void)
{
	for (auto member : members)
	{
		member.print();
	}
}

void Team::write(std::ofstream &datei)
{
	for (auto member : members)
	{
		member.writePerson(datei);
	}
}

void Team::writeCSV(std::ofstream &datei)
{
	for (auto member : members)
	{
		member.writePersonCSV(datei);
	}
}

bool Team::isGendermixValid(void)
{
	int numMen = 0;
	int numWomen = 0;
	for (auto member : members)
	{
		if (member.isMale() == true)
			numMen++;
		else
			numWomen++;
	}
	if (numWomen >= 2 && numMen >= 2)
		return true;
	else
		return false;
}

/*****Get and Set methods*****/

unsigned Team::getSize(void)
{
	return members.size();
}

float Team::getMean(void)
{
	int summe = 0;
	for (int i = 0; i < members.size(); i++)
	{
		summe += members.at(i).getStrength();
	}
	return ((float)summe / members.size());
}

float Team::getGenderMean(void)
{
	int numMen = 0, numWomen = 0;
	for (int i = 0; i < members.size(); i++)
	{
		if (members.at(i).isMale() == true)
		{
			numMen++;
		}
		else
		{
			numWomen++;
		}
	}

	if (numWomen == 0)
	{
		return 10;
	}
	else
	{
		return (float)numMen / (float)numWomen;
	}
}

int Team::getNumWomen(void)
{
	int numWomen = 0;
	for (int i = 0; i < members.size(); i++)
	{
		if (members.at(i).isMale() != true)
		{
			numWomen++;
		}
	}
	return numWomen;
}

int Team::getNumMen(void)
{
	int anzMaenner = 0;
	for (int i = 0; i < members.size(); i++)
	{
		if (members.at(i).isMale() == true)
		{
			anzMaenner++;
		}
	}
	return anzMaenner;
}