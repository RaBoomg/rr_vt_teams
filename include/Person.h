#pragma once
#include <string>

class Person
{
public:
	Person(void);
	Person(std::string _vorname, std::string _nachname, int _einordnung, bool _mann);
	~Person(void);

	void writePerson(std::ofstream &);
	void print(void);

	int getStrength(void);
	void setStrength(int _einordnung);
	std::string getFirstName(void);
	std::string getLastName(void);
	bool isMale(void);
	void setFirstName(std::string _vorname);
	void setLastName(std::string _nachname);
	void setIsMale(bool _mann);
	void writePersonCSV(std::ofstream &datei, char seperator = ',');

private:
	std::string firstName;
	std::string lastName;
	int strength;
	bool male;
};
