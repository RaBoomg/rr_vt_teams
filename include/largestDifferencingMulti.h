#include <iostream>
#include <vector>
#include <list>
#include <algorithm>
#include <numeric>
#include <iterator>
#include <functional>

void multiWayPartitioning(std::list<Person> inputList, int32_t numberOfTeams, std::list<std::list<Person>> &returnLists);