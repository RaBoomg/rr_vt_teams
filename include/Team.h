#pragma once
#include <vector>
#include <iostream>
#include <fstream>

#include "Person.h"

class Team
{
public:
	Team(void);
	~Team(void);
	void add(Person &);
	void print(void);
	void write(std::ofstream &);
	void writeCSV(std::ofstream &file);

	unsigned getSize(void);
	float getMean(void);
	bool isGendermixValid(void);
	float getGenderMean(void);
	int getNumWomen(void);
	int getNumMen(void);

private:
	std::vector<Person> members;
};
