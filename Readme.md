TODO: 
- Test more
- largestdifferencing only works when we add 100 to each players strength ?!
- Autodetect of csv seperation character
- Unit Tests?

Currently building with CMake and VSCode, needs the CMake Extension, Cpp Extension pack highly recommended

Expects a .csv with comma seperated values and line seperated data entried with a heading.
Anyone without a player strength is regarded as a non-player and not assigned to a team. 
Creates a "teams.csv" with the teams to print out 

Some links: 
https://gitlab.com/RaBoomg/rr_vt_teams


